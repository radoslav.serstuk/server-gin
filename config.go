package main

type Config struct {
	*Server        `mapstructure:"server" validate:"required"`
	OpenLibraryUrl `mapstructure:"openLibraryUrl"`
	Censors        []string `mapstructure:"censors" validate:"dive,customvalidator"`
}

type Server struct {
	Addr       string `mapstructure:"addr" validate:"hostname_port"`
	BaseApiUrl string `mapstructure:"baseApiUrl" validate:"startswith=/,endswith=/"`
	SwaggerUrl string `mapstructure:"swaggerUrl" validate:"startswith=/,endsnotwith=/"`
	MetricsUrl string `mapstructure:"metricsUrl" validate:"startswith=/,endsnotwith=/"`
}

type OpenLibraryUrl struct {
	BaseUrl     string `mapstructure:"baseUrl" validate:"url"`
	ResultLimit uint16 `mapstructure:"resultLimit" validate:"gte=0,min=1,numeric"`
}

//var Censors []string
