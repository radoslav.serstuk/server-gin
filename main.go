package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/ghodss/yaml"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"

	"github.com/go-playground/validator/v10"

	//docs "github.com/go-project-name/docs"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-gonic/gin"
	docs "gitlab.com/radoslav.serstuk/server-gin/docs"
)

var appVersion, commit, buildTime string
var versions Versions
var Censors = make(map[string]bool)
var mu sync.Mutex
var statusError int

// @BasePath /api/v1

// PingExample godoc
// @Summary ping example
// @Schemes
// @Description do ping
// @Tags example
// @Accept json
// @Produce json
// @Success 200 {string} Helloworld
// @Router /example/helloworld [get]
func Helloworld(g *gin.Context) {
	g.JSON(http.StatusOK, "helloworld")
}

var (
	requestsCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "The total number of requests to server",
	}, []string{"code", "url"})
)

func logger(c *gin.Context) {
	c.Next()

	counter, err := requestsCounter.GetMetricWithLabelValues(strconv.Itoa(c.Writer.Status()), c.Request.URL.Path)
	if err != nil {
		fmt.Println(err)
	}
	//path := c.Request.URL.Path

	//status := strconv.Itoa(c.Writer.Status())

	counter.Inc()

	/*prometheus.MustRegister(requestsCounter)
	requestsCounter.WithLabelValues(status, path).Add(1)*/
	// Process request

}

// pouzit viper na metriky, swagger,
//	struct server: <struct>
// 	addr <string>
// 	baseApiUrl <string>
// 	swaggerUrl<string>
// 	metricsUrl<string>

// openLibraryUrl: <struct>
// 	baseUrl: <string>
// 	resultLimit: <uint16>

// censors: <[]string>
// -[authorID]

var config Config

var validate *validator.Validate

func main() {

	//helm install my-helm-chart ~/helmbuild --values ~/helmbuild/values.yaml
	// helm uninstall my-helm-chart
	// helm template helm1 ~/helmbuild --set targetIngress="nginx" | less

	// k exec curl-test -n rserstuk -it -- sh
	// curl deploysvc:80/api/v1/authors?book=OL7353617M

	/*configPath := flag.String("config", "config.yaml", "load file for server")
	flag.Parse()
	fd. err := os.Open(*configPath)
	if err != nil {
		log.Fatal(err)
	}

	err := viper.ReadConfig(fd)
	*/
	validate := validator.New()

	err := validate.RegisterValidation("customvalidator", ValidateMyVal)
	if err != nil {
		log.Fatalf("registervalidationfailed")
	}
	viper.SetConfigName("config") // name of config file (without extension)
	viper.SetConfigType("yaml")   // REQUIRED if the config file does not have the extension in the name
	//viper.AddConfigPath("$HOME/server-gin/config.go") // path to look for the config file in
	//viper.AddConfigPath("$HOME/.appname") // call multiple times to add many search paths
	viper.AddConfigPath(".")   // optionally look for config in the working directory
	err = viper.ReadInConfig() // Find and read the config file
	if err != nil {            // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
	err = viper.Unmarshal(&config)
	if err != nil {
		fmt.Println("Unmarshalling failed!")
	}

	fmt.Println(config)

	err = validate.Struct(config)
	if err != nil {

		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println(err)
			return
		}

		for _, err := range err.(validator.ValidationErrors) {

			fmt.Println(err.Namespace())
			fmt.Println(err.Field())
			fmt.Println(err.StructNamespace())
			fmt.Println(err.StructField())
			fmt.Println(err.Tag())
			fmt.Println(err.ActualTag())
			fmt.Println(err.Kind())
			fmt.Println(err.Type())
			fmt.Println(err.Value())
			fmt.Println(err.Param())
			fmt.Println()
		}

		// from here you can create your own error messages in whatever language you wish
		log.Fatal("Validator failed")
		return
	}

	versions.Version = appVersion
	versions.Commit = commit
	versions.BuildTime = buildTime
	fmt.Println(versions.BuildTime, versions.Commit, versions.Version)
	router := gin.Default()

	router.Use(logger) /// middleware

	router.GET(config.MetricsUrl, gin.WrapH(promhttp.Handler())) // WrapH umozni pouzitie promthhp serveru pre router
	router.GET(config.BaseApiUrl+"v1/version", version)
	router.GET(config.BaseApiUrl+"v1/authors", getAuthors)
	router.GET(config.BaseApiUrl+"v1/books", getBooks)
	router.POST(config.BaseApiUrl+"v1/censors", enpSetCensors)

	docs.SwaggerInfo.BasePath = config.BaseApiUrl + "v1"

	router.GET(config.SwaggerUrl, ginSwagger.WrapHandler(swaggerfiles.Handler))

	v1 := router.Group(config.BaseApiUrl + "v1")
	{
		eg := v1.Group("/example")
		{
			eg.GET("/helloworld", Helloworld)
		}
	}

	srv := &http.Server{
		Addr:    config.Addr,
		Handler: router,
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	log.Print("Server Started")

	<-done
	log.Print("Server Stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second) // waits 20 sec before exit
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}

	log.Print("Server Exited Properly")

}

// @BasePath /api/v1

// PingExample godoc
// @Summary Shows an version
// @Schemes
// @Description Shows an version, commit hash, build time
// @Tags version
// @Accept json
// @Produce json
// @Success 200 {object} Versions "desc"
// @Router /version [get]
func version(c *gin.Context) {

	c.IndentedJSON(http.StatusOK, Versions{Version: versions.Version, Commit: versions.Commit, BuildTime: versions.BuildTime})
}

func getAuthors(c *gin.Context) {

	// curl http://localhost:8080/api/v1/authors?book=OL7353617M

	bookAuthors := Book{}
	name := Name{}
	id := c.Query("book")
	fmt.Println("book ", id)

	url := config.BaseUrl + "isbn/" + id + ".json"

	body, err := apiCall(url)
	if err != nil {
		log.Println(err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "failed api call"})
		return
	}

	err = json.Unmarshal(body, &bookAuthors)
	if err != nil {
		log.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "cannot create error"})

	}

	respA := make([]RespAuthor, len(bookAuthors.Authors))

	for index := range bookAuthors.Authors {
		url = config.BaseUrl + bookAuthors.Authors[index].Key + ".json"
		body, err = apiCall(url)
		if err != nil {
			c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "error"})
			return
		}
		err := json.Unmarshal(body, &name)
		if err != nil {
			log.Println("error:", err)
			c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "cannot create error"})
		}

		bookAuthors.Authors[index].Key = strings.ReplaceAll(bookAuthors.Authors[index].Key, "/authors/", "")

		respA[index].Name = name.Name
		respA[index].Key = bookAuthors.Authors[index].Key
	}

	c.IndentedJSON(http.StatusOK, respA)
}

func getBooks(c *gin.Context) {

	// curl localhost:8080/api/v1/books?author=OL1394244A
	//bookAuthors := Book{}
	//name := Name{}
	authorBooks := Entries{}

	author := c.Query("author")

	if author == "" {
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "missing URL param book which has to contain isbn of book"})
		return
	}
	if isCensored(author) {
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "this author is censored and his/her books will not be shown"})
		return
	}

	url := config.BaseUrl + "authors/" + author + "/works.json?limit=" + strconv.FormatUint(uint64(config.ResultLimit), 10)

	body, err := apiCall(url)
	if err != nil {
		log.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "cannot create error"})
		return
	}

	err = json.Unmarshal(body, &authorBooks)
	if err != nil {
		log.Println("error:", err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "cannot create error2"})

	}

	respB := make([]RespBooks, len(authorBooks.Entries))

	for index := range authorBooks.Entries {

		respB[index].Name = authorBooks.Entries[index].Title
		respB[index].Key = authorBooks.Entries[index].Key
		respB[index].Revision = authorBooks.Entries[index].Revision
		respB[index].PublishDate = authorBooks.Entries[index].Created.Value
	}

	c.IndentedJSON(http.StatusOK, respB)

}

// @BasePath /api/v1

// PingExample godoc
// @Summary Censorship
// @Schemes
// @Description Censors some bad guy by his ID
// @Tags censors
// @Accept json
// @Produce json
// @Param        id   path      string  true  "Account ID"
// @Success 200 string nice
// @Router /censors [post]
func enpSetCensors(c *gin.Context) {

	// curl  -X POST localhost:8080/api/v1/books -H 'Content-type: application/json' -d '["OL1394244A"]'
	// curl  -X POST localhost:8080/api/v1/censors -H 'Content-type: application/json' -d '["OL1394244A"]'

	//censor := c.Query("censors")
	//fmt.Println(censor)
	//reqCensors := []string{}

	err := c.BindJSON(&config.Censors)
	if err != nil {
		log.Print(err)
		c.IndentedJSON(http.StatusInternalServerError, ErrorResponse{Error: "cannot bind json"})
		return
	}
	setCensors(config.Censors)
	//c.IndentedJSON(http.StatusOK, reqCensors)
	//c.IndentedJSON(http.StatusOK, nil)

	config.Censors = make([]string, len(Censors))

	for key := range Censors {
		config.Censors = append(config.Censors, key)

	}

	newyaml, err := yaml.Marshal(config)
	if err != nil {
		fmt.Printf("err: %v\n", err)
		return
	}

	fileName := "test.yaml"
	err = ioutil.WriteFile(fileName, newyaml, 0644)
	if err != nil {
		log.Print(err, "Unable to write data into the file")
		return
	}

	c.Status(http.StatusOK)

}

func setCensors(cens []string) {
	mu.Lock()

	for _, c := range cens {
		Censors[c] = true
	}

	config.Censors = make([]string, len(Censors))
	i := 0
	for key := range Censors {
		config.Censors[i] = key
		i++
	}

	mu.Unlock()
}

func isCensored(author string) bool {
	mu.Lock()
	defer mu.Unlock()
	return Censors[author]
}

func apiCall(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()

	// !!! nacitava cely []byte do pamate, lepsie bufio.NewReader()
	body, err := ioutil.ReadAll(resp.Body)

	return body, err
}

func sendError(w http.ResponseWriter, httpCode int, err error) {
	buf, locErr := json.Marshal(ErrorResponse{Error: err.Error()})
	if locErr != nil {
		log.Println("origin error", err)
		log.Println("json marshall error", locErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(httpCode)
	w.Write(buf)
}

// merge request try
//curl localhost:8080/api/v1/books?author=OL1394244A
/*
func accessLog() {
	GetIpAddress()


}*/

func GetIpAddress(w http.ResponseWriter, r *http.Request, statusError *int) string {
	ip := r.RemoteAddr
	rUrl := r.URL
	met := r.Method
	cd := *statusError
	now := time.Now()
	fmt.Println(ip, rUrl, cd, met, now)

	return ip
}

//func middlewareEndpoint(w http.ResponseWriter, r *http.Request) {}

//	403 forbidden if auth not working
//
// vytvorime funkciu ktora bude zabalovat funkciu
//func encapsulationHandler(endpoint func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {

//}

//func basicAuth(w http.ResponseWriter, req)

/// kill Term pid prikaz na ukoncenie procesu

/*
exit := make(chan os.Signal, 1)
signal.Notify(exit, os.Interrupt, syscall.SIGTERM)
*/

// ValidateMyVal implements validator.Func
func ValidateMyVal(fl validator.FieldLevel) bool {
	r, err := regexp.Compile("^OL.*A$")
	if err != nil {
		log.Fatalf("validate custom func failed!")

	}
	return r.MatchString(fl.Field().String())

}
