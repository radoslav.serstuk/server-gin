BINARY_NAME=server
 
build:
	swag init 
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ${BINARY_NAME} -ldflags "-X 'main.appVersion=v1.0.0' -X 'main.commit=$(shell git rev-parse HEAD)' -X 'main.buildTime=$(shell date --rfc-3339=second)'" 
 
run:
	swag init 
	go build -o ${BINARY_NAME} -ldflags "-X 'main.appVersion=v1.0.0' -X 'main.commit=$(shell git rev-parse HEAD)' -X 'main.buildTime=$(shell date --rfc-3339=second)'"
	./${BINARY_NAME}

clean:
	go clean